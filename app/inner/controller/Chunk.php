<?php

declare(strict_types=1);

namespace app\inner\controller;

use app\common\controller\InnerController;
use app\common\tools\StorageTools;
use think\Request;

class Chunk extends InnerController
{
    public function readCacheChunk($chunk_md5)
    {
        $file_path = StorageTools::buildChunkCachePath($chunk_md5);

        if (!file_exists($file_path)) {
            $this->error('chunk不存在');
        }

        return file_get_contents($file_path);
    }
    public function deleteCacheChunk($chunk_md5)
    {
        $file_path = StorageTools::buildChunkCachePath($chunk_md5);

        if (!file_exists($file_path)) {
            $this->error('chunk不存在');
        }

        unlink($file_path);

        return '';
    }
}
