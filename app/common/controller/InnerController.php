<?php

namespace app\common\controller;

use app\admin\model\StorageToken;
use app\BaseController;

class InnerController extends BaseController
{
    use \app\common\traits\JumpTrait;

    public function initialize()
    {
        $token = $this->request->param('token');

        if (empty($token)) {
            $this->error('访问错误');
        }

        $model_token = StorageToken::where('token', $token)->find();

        if (empty($model_token)) {
            $this->error('访问错误');
        }

        if ($model_token->expire_time < time()) {
            $model_token->delete();
            $this->error('访问错误');
        }
    }
}
