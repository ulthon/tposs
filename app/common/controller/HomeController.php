<?php

namespace app\common\controller;

use app\admin\model\HomeUser;
use app\admin\service\ConfigService;
use app\BaseController;
use think\facade\Session;
use think\facade\View;

class HomeController extends BaseController
{
    use \app\common\traits\JumpTrait;

    public $modelUser = null;

    public function initialize()
    {
        parent::initialize();

        $username = Session::get('username');

        if (!empty($username)) {
            $model_user = HomeUser::where('username', $username)->find();
            $this->modelUser = $model_user;

        }

        $version  = env('app_debug') ? time() : ConfigService::getVersion();

        View::assign('version', $version);

        View::assign('current_user',$this->modelUser);
    }
}
