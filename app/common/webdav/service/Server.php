<?php

namespace app\common\webdav\service;

use app\common\webdav\auth\Tools;
use Sabre\DAV\Server as DAVServer;
use Sabre\DAV\Exception;
use Sabre\DAV\Version;

class Server extends DAVServer
{
    public function run()
    {

        // If nginx (pre-1.2) is used as a proxy server, and SabreDAV as an
        // origin, we must make sure we send back HTTP/1.0 if this was
        // requested.
        // This is mainly because nginx doesn't support Chunked Transfer
        // Encoding, and this forces the webserver SabreDAV is running on,
        // to buffer entire responses to calculate Content-Length.
        $this->httpResponse->setHTTPVersion($this->httpRequest->getHTTPVersion());

        // Setting the base url
        $this->httpRequest->setBaseUrl($this->getBaseUri());
        $this->invokeMethod($this->httpRequest, $this->httpResponse, false);
    }

    public function getHttpResponse()
    {
        return $this->httpResponse;
    }
}
