<?php


namespace app\common\webdav\auth\backend;

use Sabre\DAV\Auth\Backend\BackendInterface;
use Sabre\HTTP\RequestInterface;
use Sabre\HTTP\ResponseInterface;

class Easy implements BackendInterface
{
    protected $principalPrefix = 'principals/';

    protected $modelUser;

    public function __construct($model_user)
    {
        $this->modelUser = $model_user;
    }

    public function check(RequestInterface $request, ResponseInterface $response)
    {
        return [true, $this->principalPrefix . $this->modelUser->username];
    }

        /**
     * This method is called when a user could not be authenticated, and
     * authentication was required for the current request.
     *
     * This gives you the opportunity to set authentication headers. The 401
     * status code will already be set.
     *
     * In this case of Basic Auth, this would for example mean that the
     * following header needs to be set:
     *
     * $response->addHeader('WWW-Authenticate', 'Basic realm=SabreDAV');
     *
     * Keep in mind that in the case of multiple authentication backends, other
     * WWW-Authenticate headers may already have been set, and you'll want to
     * append your own WWW-Authenticate header instead of overwriting the
     * existing one.
     */
    public function challenge(RequestInterface $request, ResponseInterface $response){
        
    }
}
