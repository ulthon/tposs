<?php

namespace app\common\webdav\exception;

class ResponseException  extends \Exception
{

    protected $httpCode = 500;
    protected $httpHeaders = [];

    public function setHttpCode($code)
    {
        $this->httpCode = $code;
        return $this;
    }

    public function setHttpHeaders($headers)
    {
        $this->httpHeaders = $headers;

        return $this;
    }

    public function getHttpCode()
    {
        return $this->httpCode;
    }

    public function getHttpHeaders()
    {
        return $this->httpHeaders;
    }
}
