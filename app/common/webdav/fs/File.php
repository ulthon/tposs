<?php

declare(strict_types=1);

namespace app\common\webdav\fs;

use app\common\storage\CommonStorage;
use League\Flysystem\FileNotFoundException;
use Sabre\DAV;
use Sabre\DAV\PartialUpdate\IPatchSupport;
use think\facade\Log;

/**
 * File class.
 *
 * @copyright Copyright (C) fruux GmbH (https://fruux.com/)
 * @author Evert Pot (http://evertpot.com/)
 * @license http://sabre.io/license/ Modified BSD License
 */
class File extends Node implements DAV\IFile, IPatchSupport
{
    /**
     * Updates the data.
     *
     * @param resource $data
     */
    public function put($data)
    {

        try {
            $this->storageService->writeStreamByModel($this->currentModelPath, $data);

            $this->currentModelPath->save();

            return "\"{$this->currentModelPath->chunk_list_md5}\"";
        } catch (FileNotFoundException $th) {
        }
    }

    /**
     * Returns the data.
     *
     * @return resource
     */
    public function get()
    {
        try {

            $data = $this->storageService->readStreamByModel($this->currentModelPath);

            return $data;
        } catch (FileNotFoundException $th) {
        }
    }

    /**
     * Delete the current file.
     */
    public function delete()
    {
        $this->storageService->deleteByModel($this->currentModelPath);
    }

    /**
     * Returns the size of the node, in bytes.
     *
     * @return int
     */
    public function getSize()
    {
        try {
            return $this->currentModelPath->size;
        } catch (FileNotFoundException $th) {
        }
    }

    /**
     * Returns the ETag for a file.
     *
     * An ETag is a unique identifier representing the current version of the file. If the file changes, the ETag MUST change.
     * The ETag is an arbitrary string, but MUST be surrounded by double-quotes.
     *
     * Return null if the ETag can not effectively be determined
     *
     * @return mixed
     */
    public function getETag()
    {

        try {
            return "\"{$this->currentModelPath->chunk_list_md5}\"";
        } catch (FileNotFoundException $th) {
        }
    }

    /**
     * Returns the mime-type for a file.
     *
     * If null is returned, we'll assume application/octet-stream
     *
     * @return mixed
     */
    public function getContentType()
    {

        $content_type = null;

        if (!empty($this->currentModelPath->content_type)) {
            $content_type = $this->currentModelPath->content_type;
        }

        return $content_type;
    }


    /**
     * Updates the file based on a range specification.
     *
     * The first argument is the data, which is either a readable stream
     * resource or a string.
     *
     * The second argument is the type of update we're doing.
     * This is either:
     * * 1. append (default)
     * * 2. update based on a start byte
     * * 3. update based on an end byte
     *;
     * The third argument is the start or end byte.
     *
     * After a successful put operation, you may choose to return an ETag. The
     * ETAG must always be surrounded by double-quotes. These quotes must
     * appear in the actual string you're returning.
     *
     * Clients may use the ETag from a PUT request to later on make sure that
     * when they update the file, the contents haven't changed in the mean
     * time.
     *
     * @param resource|string $data
     * @param int             $rangeType
     * @param int             $offset
     *
     * @return string|null
     */
    public function patch($data, $rangeType, $offset = null)
    {

        Log::debug('call ptach');

        CommonStorage::patch($this->currentModelPath, $data, $rangeType, $offset);

        return $this->currentModelPath->chunk_list_md5;
    }
}
