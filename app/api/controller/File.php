<?php

declare(strict_types=1);

namespace app\api\controller;

use app\admin\model\StoragePath;
use app\common\controller\ApiController;
use app\common\storage\ReaderStorage;
use think\Request;

class File extends ApiController
{

    public function checkChunk()
    {
    }

    public function submitChunk()
    {
        # code...
    }

    public function writeChunk()
    {
        # code...
    }

    public function readChunk()
    {
        # code...
    }

    public function preview()
    {
        $path = $this->request->param('path', '/');

        $path = $this->initPath($path);

        $model_path = StoragePath::where("path", $path)->find();

        if (empty($model_path)) {
            return $this->returnMessage(['节点不存在']);
        }

        $content = ReaderStorage::readAsString($model_path);

        return download($content)->isContent(true)->mimeType($model_path->content_type)->force(false);
    }
}
