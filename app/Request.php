<?php

namespace app;

use think\facade\Log;

// 应用请求对象类
class Request extends \think\Request
{

    protected $filter = ['htmlspecialchars'];

    /**
     * 架构函数
     * @access public
     */
    public function __construct()
    {
        // 保存 php://input

        $url = $_SERVER['REQUEST_URI'] ?? '';

        if (strpos($url, '/dav') !== 0) {
            // dav 下不要读取流
            $this->input = file_get_contents('php://input');
        } else {
            $this->input = '';
        }
    }
}
