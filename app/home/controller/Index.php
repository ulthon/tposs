<?php

declare(strict_types=1);

namespace app\home\controller;

use app\admin\model\HomeUser;
use app\BaseController;
use app\common\controller\HomeController;
use app\common\webdav\auth\backend\Model;
use think\captcha\facade\Captcha;
use think\facade\Env;
use think\facade\Session;
use think\facade\View;
use think\Request;

class Index extends HomeController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {

        $captcha = Env::get('adminsystem.captcha', 1);

        View::assign('captcha', $captcha);

        return View::fetch();
    }

    /**
     * 验证码
     * @return \think\Response
     */
    public function captcha()
    {
        return Captcha::create();
    }

    public function login()
    {
        $username = $this->request->param('username');
        $password = $this->request->param('password');

        $captcha = $this->request->param('captcha');

        $type = $this->request->param('type');

        
        $model_user = HomeUser::where('username', $username)->find();
        if ($type == 'login') {
            if (!captcha_check($captcha)) {
                return json_message('验证码错误');
            }
            if (empty($model_user)) {
                return json_message('登陆失败，用户不存在');
            }
            
            if (password($password, $model_user->password_salt) != $model_user->password) {
                return json_message('登陆失败，用户密码错误');
            }
            
            Session::set('username', $username);
            
            return json_message([], 0, '登陆成功');
        } else if ($type == 'register') {
            if (!captcha_check($captcha)) {
                return json_message('验证码错误');
            }
            if (!empty($model_user)) {
                return json_message('注册失败，用户已存在');
            }

            $password_salt = uniqid();

            $password_save = password($password, $password_salt);

            $webdav_password = Model::buildDigestHash($username, $password);

            $model_user = new HomeUser();
            $model_user->username = $username;
            $model_user->password = $password_save;
            $model_user->password_salt = $password_salt;
            $model_user->nickname = $username;
            $model_user->avatar = '/static/admin/images/head.jpg';
            $model_user->home_path = uniqid();
            $model_user->webdav_password = $webdav_password;
            $model_user->save();

            Session::set('username', $username);

            return json_message([], 0, '注册成功');
        } else if ($type == 'logout') {
            Session::delete('username');
            return json_message([], 0, '退出成功');
        }
    }
    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }
}
