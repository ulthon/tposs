<?php

namespace app\admin\controller\storage;

use app\common\controller\AdminController;
use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use think\App;

/**
 * @ControllerAnnotation(title="storage_path")
 */
class Path extends AdminController
{

    use \app\admin\traits\Curd;

    public function __construct(App $app)
    {
        parent::__construct($app);

        $this->model = new \app\admin\model\StoragePath();
        
        $this->assign('select_list_type', $this->model::SELECT_LIST_TYPE, true);

    }

    
}