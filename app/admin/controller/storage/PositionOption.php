<?php

declare(strict_types=1);

namespace app\admin\controller\storage;

use app\admin\model\StoragePosition;
use app\common\controller\AdminController;
use League\Flysystem\Filesystem;
use think\Request;

class PositionOption extends AdminController
{

    /**
     * Undocumented variable
     *
     * @var Filesystem
     */
    protected $filesystem;

    public function initialize()
    {
        parent::initialize();
        $id = $this->request->param('id');
        $model_position = StoragePosition::find($id);

        $file_handler = $model_position->file_handler;

        $file_system = $file_handler->getFlysystem();
        $this->fileSystem = $file_system;
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index($id)
    {
        //
        $path = $this->request->param('path', '');


        $parent_path = '';

        $path_info = explode('/', $path);

        array_pop($path_info);

        $parent_path = implode('/', $path_info);

        $file_system = $this->fileSystem;

        if (!empty($path)) {
            $is_exits = $this->fileSystem->has($path);

            if (!$is_exits) {
                $this->error('当前目录不存在，返回上一级目录', '', url('index', ['id' => $id, 'path' => $parent_path]));
            }
        }

        $list_content = $file_system->listContents($path);
        $this->assign('list_content', $list_content);
        $this->assign('parent_path', $parent_path);
        return $this->fetch();
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id, $path)
    {
        //

        $content = $this->fileSystem->read($path);

        return download($content)->name(basename($path), false)->isContent(true);
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id, $path, $type)
    {
        //

        if ($type == 'dir') {
            $this->fileSystem->deleteDir($path);
        } else if ($type == 'file') {
            $this->fileSystem->delete($path);
        } else if ($type == 'clear') {
            $list_content = $this->fileSystem->listContents($path);
            foreach ($list_content as $item_file) {
                if ($item_file['type'] == 'dir') {
                    $this->fileSystem->deleteDir($item_file['path']);
                } else if ($item_file['type'] == 'file') {
                    $this->fileSystem->delete($item_file['path']);
                }
            }
        }

        return $this->success('删除成功');
    }
}
