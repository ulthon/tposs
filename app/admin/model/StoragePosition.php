<?php

namespace app\admin\model;

use app\common\model\ConfigModel;
use app\common\model\TimeModel;
use app\common\tools\FileHandlerTools;

/**
 * 
 * @property FileHandlerTools $file_handler
 */
class StoragePosition extends TimeModel
{

    protected $name = "storage_position";

    protected $deleteTime = "delete_time";


    public const SELECT_LIST_TYPE = ['local' => '本机', 'sftp' => 'SFTP', 'ftp' => 'FTP', 'qiniu' => '七牛云', 'site' => '当前站点',];

    public const SELECT_LIST_STATUS = ['enable' => '启用', 'disabled' => '禁用',];

    public function storagePositionGroup()
    {
        return $this->belongsTo(StoragePositionGroup::class, 'group_id');
    }

    public function getMd5Attr()
    {
        return md5($this->getAttr('id'));
    }

    public function setConfigAttr($value)
    {
        return json_encode($value);
    }


    public function getConfigAttr($value)
    {

        if (empty($value)) {
            return [];
        } else {
            $value = json_decode($value, true);
        }

        return ConfigModel::create($value);
    }

    public function getFileHandlerAttr()
    {

        return FileHandlerTools::buildInstance($this);
    }
}
