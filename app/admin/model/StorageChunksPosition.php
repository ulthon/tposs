<?php

namespace app\admin\model;

use app\common\model\TimeModel;

class StorageChunksPosition extends TimeModel
{

    protected $name = "storage_chunks_position";

    protected $deleteTime = false;

    public function storagePosition()
    {
        return $this->belongsTo(StoragePosition::class, 'storage_position_id');
    }
}
