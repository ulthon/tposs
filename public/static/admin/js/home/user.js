define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'home.user/index',
        add_url: 'home.user/add',
        edit_url: 'home.user/edit',
        delete_url: 'home.user/delete',
        export_url: 'home.user/export',
        modify_url: 'home.user/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'id'},
                    {field: 'create_time', title: '创建时间'},
                    {field: 'username', title: '用户名'},
                    {field: 'nickname', title: '昵称'},
                    {field: 'avatar', title: '头像', templet: ea.table.image},
                    {field: 'home_path', title: '目录路径'},
                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});