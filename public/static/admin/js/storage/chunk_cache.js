define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'storage.chunk_cache/index',
        add_url: 'storage.chunk_cache/add',
        edit_url: 'storage.chunk_cache/edit',
        delete_url: 'storage.chunk_cache/delete',
        export_url: 'storage.chunk_cache/export',
        modify_url: 'storage.chunk_cache/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},                    {field: 'id', title: 'id'},                    {field: 'chunk_md5', title: 'chunk_md5'},                    {field: 'host_key', title: '节点名称'},                    {field: 'host_domain', title: '节点域名，带http'},                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});