define(["jquery", "easy-admin", 'vue'], function ($, ea, Vue) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'storage.position/index',
        add_url: 'storage.position/add',
        edit_url: 'storage.position/edit',
        delete_url: 'storage.position/delete',
        export_url: 'storage.position/export',
        modify_url: 'storage.position/modify',
    };
    var app;

    var initForm = function () {
        app = new Vue({
            el: '#app-form',
            data() {
                return {
                    configType: $('select[name="type"]').val()
                }
            },
            mounted() {
                ea.listen();
                layui.form.on('select(config-type)', (data) => {

                    this.configType = data.value;

                    this.$nextTick(() => {
                        ea.listen();
                    })

                })
            }
        })
    }

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    { type: 'checkbox' },
                    { field: 'id', title: 'id' },
                    { field: 'create_time', title: '创建时间' },
                    { field: 'storagePositionGroup.title', title: '分组' },
                    { field: 'type', search: 'select', selectList: ea.getDataBrage('select_list_type'), title: '类型' },
                    { field: 'status', search: 'select', selectList: ea.getDataBrage('select_list_status'), title: '状态' },
                    { field: 'size_limit', title: '存储空间限制' },
                    { field: 'weight', title: '权重' },
                    { field: 'size_used', title: '已使用空间' },
                    {
                        width: 250, title: '操作', templet: ea.table.tool, operat: [
                            'edit',
                            [
                                {
                                    text: '测试读写',
                                    url: 'storage.position/testStorage',
                                    method: 'open',
                                    class: 'layui-btn layui-btn-xs layui-btn-success',
                                    field: 'id',
                                    extend: 'data-full="true"',
                                },
                                {
                                    text: '存储管理',
                                    url: 'storage.position_option/index',
                                    method: 'open',
                                    class: 'layui-btn layui-btn-xs layui-btn-success',
                                    field: 'id',
                                    extend: 'data-full="true"',
                                },
                            ],
                            'delete'
                        ]
                    },

                ]],
            });

            ea.listen();
        },
        add: function () {
            initForm()

        },
        edit: function () {
            initForm()

        },
        testStorage: function () {
            app = new Vue({
                el: '#test-app',
                data() {
                    return {
                        listTestFunction: [
                            {
                                title: '写入',
                                type: 'write',
                                result: '待测试',
                                time: ''
                            },
                            {
                                title: '读取',
                                type: 'read',
                                result: '待测试',
                                time: ''
                            },
                            {
                                title: '删除',
                                type: 'delete',
                                result: '待测试',
                                time: ''
                            },
                        ]
                    }
                },
                mounted() {

                },
                methods: {
                    startTest(size) {
                        loading.show(this.listTestFunction.length)
                        this.listTestFunction.forEach((item) => {

                            $.ajax({
                                url: '',
                                async: false,
                                data: {
                                    type: item.type,
                                    size: size
                                },
                                success: (result) => {
                                    loading.hide()


                                    item.result = result.data.msg;
                                    item.time = result.data.time;
                                    item.path = result.data.path;
                                    item.size = result.data.size;
                                    item.rate = result.data.rate;
                                    item.total_time = result.data.total_time;
                                }
                            })

                        })
                    }
                }
            })

        }
    };
    return Controller;
});