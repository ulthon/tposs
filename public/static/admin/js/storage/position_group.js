define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'storage.position_group/index',
        add_url: 'storage.position_group/add',
        edit_url: 'storage.position_group/edit',
        delete_url: 'storage.position_group/delete',
        export_url: 'storage.position_group/export',
        modify_url: 'storage.position_group/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},                    {field: 'id', title: 'id'},                    {field: 'create_time', title: '创建时间'},                    {field: 'title', title: '名称'},                    {field: 'weight_write', title: '写入权重'},                    {field: 'weight_read', title: '读取权重'},                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});