define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'storage.path/index',
        add_url: 'storage.path/add',
        edit_url: 'storage.path/edit',
        delete_url: 'storage.path/delete',
        export_url: 'storage.path/export',
        modify_url: 'storage.path/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    { type: 'checkbox' },
                    { field: 'id', title: 'id' },
                    { field: 'create_time', title: '创建时间' },
                    { field: 'path', title: '文件路径', align: 'left', minWidth: '480' },
                    { field: 'type', search: 'select', selectList: ea.getDataBrage('select_list_type'), title: '文件类型' },
                    { field: 'user_id', title: '用户' },
                    { field: 'size', title: '文件大小' },
                    { field: 'pid', title: '所属目录ID' },
                    { field: 'properties', title: '属性' },
                    { width: 250, title: '操作', templet: ea.table.tool, fixed: 'right' },

                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});