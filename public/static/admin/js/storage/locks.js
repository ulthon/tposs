define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'storage.locks/index',
        add_url: 'storage.locks/add',
        edit_url: 'storage.locks/edit',
        delete_url: 'storage.locks/delete',
        export_url: 'storage.locks/export',
        modify_url: 'storage.locks/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},                    {field: 'id', title: 'id'},                    {field: 'owner', title: 'owner'},                    {field: 'timeout', title: 'timeout'},                    {field: 'created', title: 'created'},                    {field: 'token', title: 'token'},                    {field: 'scope', title: 'scope'},                    {field: 'depth', title: 'depth'},                    {field: 'uri', title: 'uri'},                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});