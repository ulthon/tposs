<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use app\common\storage\ReaderStorage;
use app\common\webdav\IndexWebdav;
use app\Request;
use think\facade\App;
use think\facade\Log;
use think\facade\Route;
use think\facade\View;



// 明确指定以下方法和路径进入webdav根目录
$root_webdav_route = [
    '/',
    '/desktop.ini'
];

$method_webdav_route = [
    'OPTIONS',
    'PROPFIND'
];

foreach ($root_webdav_route as  $webdav_root) {
    foreach ($method_webdav_route as  $webdav_method) {
        Route::rule($webdav_root, function (Request $request) {
            $webdav = new IndexWebdav($request);
            return $webdav->run('root');
        }, $webdav_method);
    }
}

Route::rule('/', function (Request $request) {
    return View::fetch(App::getRootPath() . 'view/index.html');
});


Route::rule('dav', function (Request $request) {
    $webdav = new IndexWebdav($request);
    return $webdav->run('dav');
}, 'OPTIONS');

Route::rule('dav', function (Request $request) {
    $webdav = new IndexWebdav($request);
    return $webdav->run('dav');
});